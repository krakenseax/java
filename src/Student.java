//Write a program that will create an array of 10 student objects, input the id and
//marks of 10 students and display the details of the student object that has the highest marks
import java.util.Scanner;
import java.util.Random;
class Student {
    private int id;
    private double marks;
    private static int numStudents=0;

    public Student()
    {
        this.id=0;
        this.marks=0;
        numStudents++;
    }

    public Student(int id, double marks)
    {
        this.id=id;
        this.marks=marks;
        numStudents++;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public double getMarks() {
        return marks;
    }
    public void setMarks(double marks) {
        this.marks = marks;
    }

    public int getNumStudents() {
        return numStudents;
    }

    public String toString()
    {
        String str="id:"+this.id+", marks="+this.marks;
        return str;
    }

    public static void main(String[] args) {
        Student[] studentArray = new Student[10];
        Random rand = new Random();
        int id = 1700000;

        for (int i = 0; i < 10; i++) {
            double marks = rand.nextInt(101);
            id++;
            studentArray[i] = new Student(id, marks);
        }

        int posHigh = 0;
        double maxMarks = -1;

        for (int i = 0; i < 10; i++) {
            System.out.println("Student " + studentArray[i]);
            if (studentArray[i].getMarks() > maxMarks) {
                maxMarks = studentArray[i].getMarks();
                posHigh = i;
            }
        }

        System.out.println();
        System.out.print(studentArray[posHigh]);

    }

}