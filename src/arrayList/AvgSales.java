package arrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class AvgSales {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float sum = 0, average = 0;
        ArrayList<Float> monthlySales  = new ArrayList<Float>(100);
        String [] month = {"January", "Febuary", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};

        System.out.println("Enter monthly sales income for 2014");
        System.out.println("-------------------------------------------");
        for (int i = 0; i < month.length; i++) {
            System.out.print("Enter income for " + month[i] + ": ");
            float income = input.nextFloat();
            monthlySales.add(income);
        }

        for (int i = 0; i < monthlySales.size(); i++) {
            sum += monthlySales.get(i);
        }

        average = sum / 12;
        System.out.print("Average sales revenue for 2014 is: " + average);
    }
}
