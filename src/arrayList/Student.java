package arrayList;

public class Student {
    int studentID;
    double marks;

    public Student() {
        this.marks = 0;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public double getMarks() {
        return marks;
    }

    public void setMarks(double marks) {
        this.marks = marks;
    }

    public Student(int studentID, double marks) {
        this.studentID = studentID;

        this.marks = marks;
    }

    public String toString() {
        return ("StudentID: " + getStudentID() + ", Student Marks: " + getMarks());
    }
}
