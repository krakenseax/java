package arrayList;

import java.util.ArrayList;
import java.util.Random;

public class arrayList {
    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<Integer>(10);
        Random rand = new Random();
        int n = rand.nextInt(100);
        for (int i = 0; i < n; i++) {
            myList.add(rand.nextInt(1000));
        }

        System.out.println("There are " + n + " numbers in arraylist");

        for (int i =0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }

        System.out.println();
    }
}
