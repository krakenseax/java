package arrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class findWord {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> mySentence = new ArrayList<String>(50);
        boolean found = false;
        System.out.print("Enter a sentence: ");
        String sentence = input.nextLine();
        //Time to purge! Let's cleanse up a bit: removing any possible leading or trailing spaces with .trim()
        String sentenceTrimmed = sentence.trim();
        //Regex chained with .length method to count characters before and after spaces
        //It seems that this counts any character with spaces before and after them, including special characters
        int numWords = sentenceTrimmed.split("\\s+").length;
        //Regex is my friend <3 : Cleaning up the sentence by getting rid of special characters to store only words in the String[] array.
        //People probably don't use that many characters in a sentence
        //Who cares anyway
        // <3 Regex
        String[] textArray = sentenceTrimmed.split("[\\s@&.?!$+#%*=]+");
        //I could have just used the above method directly inside the loop conditions but whatever...
        for (int i = 0; i < numWords; i++) {
            mySentence.add(textArray[i]);
        }

        System.out.print("Enter a word to search because using ctrl + f is too mainstream: ");
        String search = input.nextLine();

        for (int i = 0; i < mySentence.size(); i++) {
            if (search.equals(mySentence.get(i))) {
                found = true;
            }
        }

        if (found) {
            System.out.print("Ye m8, you entered that earlier");
        } else {
            System.out.print("No, you haven't used this word before, probably too complicated for your vocabulary list");
        }
    }
}
