package arrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class studentTest {

    public static void main(String[] args) {
        double highest = -999999, lowest = 999999;
        int highId = 0, lowID = 0;
        Scanner input = new Scanner(System.in);
        ArrayList<Student> studentList = new ArrayList<Student>(98);
        System.out.print("Enter number of students: ");
        int numStudents = input.nextInt();

        for (int i = 0; i < numStudents; i++) {
            System.out.print("Enter StudentID between 701-799: ");
            int studentId = input.nextInt();
            if (studentId < 701 || studentId > 799) {
                do {
                    System.out.print("Error! Enter StudentID between 701-799: ");
                    studentId = input.nextInt();
                } while(studentId < 701 || studentId > 799);
            }

            System.out.print("Enter Student Marks between 0.0-100.0: ");
            double studentMark = input.nextDouble();
            if (studentMark < 0.0 || studentMark > 100.0) {
                do {
                    System.out.print("Error! Enter StudentID between 701-799: ");
                    studentMark = input.nextInt();
                } while(studentMark < 0.0 || studentMark > 100.0);
            }

            Student studentObj = new Student(studentId, studentMark);

            studentList.add(studentObj);
        }

        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).getMarks() < lowest) {
                lowest = studentList.get(i).getMarks();
                lowID = studentList.get(i).getStudentID();
            }

            if (studentList.get(i).getMarks() > highest) {
                highest = studentList.get(i).getMarks();
                highId = studentList.get(i).getStudentID();
            }
        }

        System.out.println("Highest mark of " + highest + " was scored by Student#" + highId);
        System.out.print("Lowest mark of " + lowest + " was scored by Student#" + lowID);
    }
}
