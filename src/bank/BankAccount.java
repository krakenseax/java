package bank;

public class BankAccount {
    private String acc_name;
    private int acc_num;
    private double acc_balance;

    public BankAccount(String acc_name, int acc_num, double acc_balance) {
        this.acc_name = acc_name;
        this.acc_num = acc_num;
        this.acc_balance = acc_balance;
    }

    public String getAcc_name() {
        return acc_name;
    }

    public int getAcc_num() {
        return acc_num;
    }

    public double getAcc_balance() {
        return acc_balance;
    }

    public void deposit(double amount) {
        this.acc_balance += amount;
    }

    public void withdraw(double amount) {
        if (amount < this.acc_balance) {
            this.acc_balance -= amount;
        } else {
            System.out.print("Not enough balance for withdrawal");
        }
    }

    public String toString() {
        String str = "Account holder name: " + this.acc_name +
                ", account no: " +this.acc_num +
                ", account balance: " + this.acc_balance;
        return str;
    }
}


