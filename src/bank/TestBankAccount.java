package bank;

import java.util.Scanner;

public class TestBankAccount {
    public static BankAccount inputBankAccount() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter Bank Account Name: ");
        String name = input.nextLine();

        System.out.print("Enter Bank Account Number: ");
        int number = input.nextInt();

        System.out.print("Enter Bank account Balance: ");
        double balance = input.nextDouble();

        BankAccount newAccount = new BankAccount(name, number, balance);

        return newAccount;
    }

    public static void displayBankAccounts(BankAccount [] accArr) {
        for (int i = 0; i < accArr.length; i++) {
            System.out.print("Account Name: " + accArr[i].getAcc_name());
            System.out.print("Account Number: " +accArr[i].getAcc_num());
            System.out.print("Account Balance: " +accArr[i].getAcc_balance());
        }
    }

    public static BankAccount searchBankAccount(BankAccount [] accArr, int accNum) {
        BankAccount correspondingAccount = null;
        for (int i = 0; i < accArr.length; i++) {
            if (accArr[i].getAcc_num() == accNum) {
                correspondingAccount = accArr[i];
            }
        }

        return correspondingAccount;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BankAccount [] bankArr = new BankAccount[2];
        for (int i = 0; i < 2; i++) {
            bankArr[i] = inputBankAccount();
        }
            System.out.println("------------------------------------------------------");

            System.out.print("Enter account number: ");
            int accountNum = input.nextInt();

            System.out.println("Enter the corresponding number for an operation: ");
            System.out.println("1. Deposit");
            System.out.println("2. Withdraw");
            System.out.print("Choice: ");
            int choice = input.nextInt();

            System.out.print("Enter amount: ");
            double amount = input.nextDouble();

            if (searchBankAccount(bankArr, accountNum) != null) {

                if (choice == 1) {
                    searchBankAccount(bankArr, accountNum).deposit(amount);
                    System.out.print(searchBankAccount(bankArr, accountNum).toString());
                } else {
                    searchBankAccount(bankArr, accountNum).withdraw(amount);
                    System.out.print(searchBankAccount(bankArr, accountNum).toString());
                }
            } else {
                System.out.println("Wrong Account Number!");
            }
    }
}
