package calcPtDist;

public class Point{
    private int x,y;
    public Point(int x, int y){
        this.x=x;
        this.y=y;
    }
    public int get_x(){
        return this.x;
    }
    public int get_y(){
        return this.y;
    }
    public String toString(){
        return "("+this.x
                +","+this.y+")";
    }
}