package date;

import java.util.Date;
import java.util.Scanner;

public class MyDate {
    int month, day, year;

    private MyDate() {
        this.month = 1;
        this.day = 1;
        this.year = 2001;
    }

    public MyDate(int month, int day, int year) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public void displayAmerican() {
        System.out.println(month + "/" + day + "/" + year);
    }

    public void displayVerboseAmerican() {
        String[] months = {"", "January", "Febuary", "March", "April", "May", "June",
                           "July", "August", "September", "October", "November", "December"};

        System.out.println(months[month] + "/" + day + "/" + year);
    }

    public void displayVerboseBritish() {
        String[] months = {"", "January", "Febuary", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};

        System.out.println(day + "/" + months[month] + "/" + year);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int dd, mm, yy;

        System.out.print("Input a day(1-31): ");
        dd = input.nextInt();
        while (dd  < 1 || dd > 31) {
            System.out.print("Wrong value for day, input again(1-31): ");
            dd = input.nextInt();
        }

        System.out.print("Input a  month(1-12): ");
        mm = input.nextInt();
        while (mm  < 1 || mm > 12) {
            System.out.print("Wrong value for month, input again(1-12): ");
            dd = input.nextInt();
        }

        System.out.print("Input a  year(1-12): ");
        yy = input.nextInt();
        while (yy  < 1950 || yy > 2020) {
            System.out.print("Wrong value for month, input again(1950-2020): ");
            yy = input.nextInt();
        }

        //d1.displayAmerican();

    }
}

