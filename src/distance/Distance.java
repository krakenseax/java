package distance;

public class Distance {
    int feet, inches;

    public Distance() {
        this.feet = 0;
        this.inches = 0;
    }

    public void Input(int feet, int inches) {
        this.feet = feet;
        this. inches = inches;
    }

    public void Add(int feet, int inches, int feet2, int inches2) {
        this.feet = feet + feet2;
        this.inches = inches + inches2;
        if(this.inches >= 12) {
            this.inches = this.inches - 12;
            this.feet = this.feet++;
        }
    }

    public void Output() {
        System.out.println("feet: " + this.feet);
        System.out.println("inches: " + this.inches);
    }
}
