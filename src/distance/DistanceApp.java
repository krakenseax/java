package distance;
import java.util.Scanner;

public class DistanceApp {


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in) ;
        System.out.println("Distance 1");

        System.out.print("Enter the feet: ");
        int feet = input.nextInt();

        System.out.print("Enter the inches: ");
        int inches = input.nextInt();

        Distance inputDist = new Distance();
        inputDist.Input(feet, inches);

        System.out.println("Distance 2");

        System.out.print("Enter the feet: ");
        int feet2 = input.nextInt();

        System.out.print("Enter the inches: ");
        int inches2 = input.nextInt();

        Distance inputDist2 = new Distance();
        inputDist2.Input(feet2, inches2);

        System.out.println("The sum of the two distances is:");
        inputDist.Add(feet, inches, feet2, inches2);
        inputDist.Output();
    }
}
