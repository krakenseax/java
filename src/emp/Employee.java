package emp;

public class Employee {
    String name;
    int age;
    String designation;
    double salary;
    // This is the constructor of the class Employee
    public Employee(String name) {
        this.name = name;
        this.age=0;
        this.designation= "";
        this.salary= 0;
    }

    // Assign the age of the Employee to the parameter age.
    public void setEmpAge(int empAge){
        age = empAge;
    }
    /* Assign the designation to the parameter designation.*/
    public void setEmpDesignation(String empDesig){
        designation = empDesig;
    }
    /* Assign the salary to the parameter salary.*/
    public void setEmpSalary(double empSalary){
        salary = empSalary;
    }
    /* Print the Employee details    */
    public void printEmployee(){
        System.out.println("Name:" + name);
        System.out.println("Age:" + age);
        System.out.println("Designation:" + designation);
        System.out.println("Salary:" + salary);
    }
}
