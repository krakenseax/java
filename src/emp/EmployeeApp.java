package emp;

public class EmployeeApp {
    public static void main(String[] args) {
        // Create two different Employee objects; E1 and E2
        // Employee name has already been passed on as parameter in the contructor
        // so data is passed on directly as parameter here
        Employee E1 = new Employee("James Smith");
        Employee E2 = new Employee("Mary Anne");

        // Invoke the methods to pass on data for object E1
        E1.setEmpAge(26);
        E1.setEmpDesignation("Senior Software Engineer");
        E1.setEmpSalary(40000);

        // Invoke the methods to pass on data for object E2
        E2.setEmpAge(21);
        E2.setEmpDesignation("Software Engineer");
        E2.setEmpSalary(20000);

        // Invoke printEmployee methods on both objects to display their data
        System.out.println("E1");
        E1.printEmployee();
        System.out.println("E2");
        E2.printEmployee();
    }
}
