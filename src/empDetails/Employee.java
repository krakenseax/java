package empDetails;

public class Employee {
    private int empId;
    private String empName;
    private String empDept;
    private Double empSalary;

    // Constructor for object initialisation
    public Employee(String empName) {
        this.empId = 00000;
        this.empName = empName;
        this.empDept = "";
        this.empSalary = 0.0;
    }

    // Constructor for default object
    public Employee() {
        this("Details not assigned yet!");
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpDept() {
        return empDept;
    }

    public void setEmpDept(String empDept) {
        this.empDept = empDept;
    }

    public Double getEmpSalary() {
        return empSalary;
    }

    public void setEmpSalary(Double empSalary) {
        this.empSalary = empSalary;
    }

    public void displayDetails() {
        System.out.println("Employee ID: " + this.empId);
        System.out.println("Employee Name: " + this.empName);
        System.out.println("Employee Department: " + this.empDept);
        System.out.println("Employee Salary: " + this.empSalary);
    }
}
