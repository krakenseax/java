package empDetails;
import java.util.Scanner;
public class TestEmployee {
    public static Employee inputEmployee(int i) {
        Scanner input = new Scanner(System.in);

        System.out.println("Employee #" + (i + 1));
        System.out.print("Enter ID: ");
        int empID = input.nextInt();

        System.out.print("Enter Name: ");
        String empName = input.next();

        System.out.print("Enter Department:" );
        String empDept = input.next();

        System.out.print("Enter Salary: ");
        Double empSalary = input.nextDouble();
        System.out.println("-----------------------");

        Employee employeeDetails = new Employee();
        employeeDetails.setEmpId(empID);
        employeeDetails.setEmpName(empName);
        employeeDetails.setEmpDept(empDept);
        employeeDetails.setEmpSalary(empSalary);

        return employeeDetails;
    }

    public static Employee highestSalary(Employee[] empArray) {
        Employee highestSalaryObj = null;
        Double highest = -99999.9;
        for (int i = 0; i < empArray.length; i++) {
            if (empArray[i].getEmpSalary() > highest) {
                highest = empArray[i].getEmpSalary();
                highestSalaryObj = empArray[i];
            }
        }
        return highestSalaryObj;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter number of employees to register: ");
        int num = input.nextInt();
        Employee [] empArray = new Employee[num];

        System.out.println("Input Employee Details");
        System.out.println("-----------------------");
        for (int i = 0; i < num; i++) {
            empArray[i] = inputEmployee(i);
        }
        System.out.println("-----------------------------------");
        System.out.println("Highest earning employee details:");
        System.out.println("-----------------------------------");
        highestSalary(empArray).displayDetails();
    }
}
