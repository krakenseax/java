package ex1;

public class Complex {
	private int real;
	private int imaginary;

	public Complex(){
		this.real=0;
		this.imaginary=0;
	}

	public Complex(int real, int imaginary) {
		this.real=real;
		this.imaginary=imaginary;
	}
	
	//Accessor for real attribute
	public int get_real() {	//get_real
		return this.real;
	}
	
	//mutator method for attribute real
	public void set_real(int real) {
		this.real=real;
	}
	
	//Accessor for attribute imaginary
	public int get_imaginary(){
		return this.imaginary;
	}
	
	//Mutator for attribute imaginary
	public void set_imaginary(int imaginary) {
		this.imaginary=imaginary;
	}
	
	public void display(){
		System.out.println(this.real+"+"+this.imaginary+"i");
	}

	
}
