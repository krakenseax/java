package ex1;

public class ComplexApp {

	public static Complex add(Complex c1, Complex c2) {
		Complex result=new Complex();
		int new_real=c1.get_real()+c2.get_real();
		int new_imaginary=c1.get_imaginary()+c2.get_imaginary();

		result.set_real(new_real);
		result.set_imaginary(new_imaginary);
		return result;
	}

	public static Complex subtract(Complex c1, Complex c2) {
		Complex result=new Complex();
		int new_real=c1.get_real()-c2.get_real();
		int new_imaginary=c1.get_imaginary()-c2.get_imaginary();

		result.set_real(new_real);
		result.set_imaginary(new_imaginary);
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Complex comp1=new Complex(7,3);
		Complex comp2=new Complex();

		comp2.set_real(3);
		comp2.set_imaginary(2);
		//Complex c2=new Complex(3,2);

		Complex addResult=add(comp1,comp2);
		Complex subResult=subtract(comp1,comp2);

		System.out.print("Addition result=");
		addResult.display();
		System.out.print("Subtraction result=");
		subResult.display();

	}

}
