package linkedList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class catenate {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        LinkedList<String> left = new LinkedList<>();
        LinkedList<String> right = new LinkedList<>();

        left.add("Hans Zimmer");
        left.add("Leonardo Di Caprio");
        left.add("Ellen Page");

        System.out.print("Enter number of items to store in list: ");
        int num = input.nextInt();

        for (int i = 0; i < num; i++) {
            System.out.print("Enter item#" + i + ": "  );
            String item = input.next();
            right.add(item);
            left.add(right.get(i));
        }

        Iterator<String> itr = left.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
