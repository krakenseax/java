package linkedList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class readList {
    public static void main(String[] args) {
        int numEl;
        Scanner input = new Scanner(System.in);
        LinkedList<Integer> inputList = new LinkedList();

        System.out.print("Enter a number of items to enter: ");
        numEl = input.nextInt();
        if (numEl < 0) {
            do {
                System.out.print("Error! Enter a number greater than 0: ");
                numEl = input.nextInt();
            } while(numEl < 0);
        }

        for (int i = 0; i < numEl; i++) {
            System.out.print("Enter an integer to add to the list: ");
            int num = input.nextInt();
            inputList.add(num);
        }

        Iterator<Integer> listItr = inputList.iterator();
        while (listItr.hasNext()) {
            System.out.println(listItr.next());
        }
    }
}
