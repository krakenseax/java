package linkedList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class sum {
    public static void main(String[] args) {
        int sum = 0;
        Scanner input = new Scanner(System.in);
        LinkedList <Integer> in = new LinkedList();
        LinkedList <Integer> out = new LinkedList();

        System.out.print("Enter number of elements: ");
        int elements = input.nextInt();

        for (int i = 0; i < elements; i++) {
            System.out.print("Enter an integer: ");
            int number = input.nextInt();
            in.add(i,number);
        }

        for (int i = 0; i < elements; i++) {
            sum += in.get(i);
            out.add(i, sum);
        }

        Iterator <Integer> readOutItr = out.iterator();
        while (readOutItr.hasNext()) {
            System.out.println(readOutItr.next());
        }
    }
}
