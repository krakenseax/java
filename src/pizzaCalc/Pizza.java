package pizzaCalc;

public class Pizza {
    private int diameter;
    private double area;
    private double cost_sq_cm;
    private double price;

    public Pizza(){
        this.diameter=1;
        this.cost_sq_cm=0;
        this.price=0;
    }
    public int getDiameter(){
        return this.diameter;
    }
    public void setPizzaDiameter(int PizDiameter){
        this.diameter = PizDiameter;
    }
    public double getcost_sq_cm(){
        return this.cost_sq_cm;
    }
    public void setcost_sq_cm(double Pizcost_sq_cm){
        this.cost_sq_cm = Pizcost_sq_cm;
    }
    public double calcArea(){
        this.area = 3.142 * (this.diameter * this.diameter/4);
        return this.area;
    }
    public double getPrice(){
        return this.price = this.area * this.cost_sq_cm;
    }
}
