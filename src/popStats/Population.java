package popStats;

public class Population {
    private int currentPopulation, yearlyDeaths, yearlyBirths;

    // Constructor for object initialisation
    public Population(int currentPopulation, int yearlyDeaths, int yearlyBirths) {
        this.currentPopulation = currentPopulation;
        this.yearlyDeaths = yearlyDeaths;
        this.yearlyBirths = yearlyBirths;
    }

    public void setPopulation(int currentPopulation) {
        if (currentPopulation < 2) {
            this.currentPopulation = 2;
        } else {
            this.currentPopulation = currentPopulation;
        }
    }

    public void setDeaths(int yearlyDeaths) {
        if (yearlyDeaths < 0) {
            this.yearlyDeaths = 0;
        } else {
            this.yearlyDeaths = yearlyDeaths;
        }
    }

    public void setBirths(int yearlyBirths) {
        if (yearlyBirths < 0) {
            this.yearlyBirths = 0;
        } else {
            this.yearlyBirths = yearlyBirths;
        }
    }

    public double getBirthRate() {
        return (double)(yearlyBirths / currentPopulation);
    }

    public  double getDeathRate() {
        return (double)(yearlyDeaths / currentPopulation);
    }
}
