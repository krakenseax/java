package popStats;
import java.util.Scanner;

public class TestPopulation {
    public static Population inputStats() {
        Scanner input = new Scanner(System.in);
        Population populationStats = new Population(2, 0, 0);

        System.out.print("Enter current population: ");
        int currentPop = input.nextInt();

        System.out.print("Enter no. of Births: ");
        int currentBirths = input.nextInt();

        System.out.print("Enter no. of Deaths: ");
        int currentDeaths = input.nextInt();

        populationStats.setPopulation(currentPop);
        populationStats.setBirths(currentBirths);
        populationStats.setDeaths(currentDeaths);

        return populationStats;
    }

    public static void main(String[] args) {
        inputStats();
        System.out.print("Birth Rate: " + inputStats().getBirthRate());
        System.out.print("Death Rate: " + inputStats().getDeathRate());
    }
}
