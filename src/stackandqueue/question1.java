package stackandqueue;
import    java.util.Scanner;
import    java.util.Stack;
public class question1 {
    // This algorithm is converting n to binary representation
        public static void  main(String[] args){
            Scanner input=new Scanner(System.in);
            Stack<Integer> stack = new Stack<Integer>();
            int N;
            System.out.print("Input N:");
            N=input.nextInt();
            while (N > 0)
            {
                stack.push(N % 2);
                N  = N / 2;
            }
            while(!stack.empty()){
                System.out.print(stack.pop());
            }
            System.out.println();
        }
}