package stackandqueue;
import    java.util.LinkedList;
import    java.util.Queue;
import    java.util.Stack;
public class question2 {
    public static void main(String[] args) {
        //This program is printing out elements of the stack in a FIFO order
        Queue<String> q = new LinkedList<String>();
        q.add("One");
        q.add("Two");
        q.add("Three");
        q.add("Four");
        q.add("Five");
        Stack<String> stack = new Stack<String>();
        while (!q.isEmpty())
            stack.push(q.remove());
        while (!stack.isEmpty())
            q.add(stack.pop());

        while(!q.isEmpty())
            System.out.print(q.remove()+"    ");
        System.out.println();
    }
}
