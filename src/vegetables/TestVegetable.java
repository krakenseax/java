package vegetables;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TestVegetable {
    public static Vegetable inputVegetable() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter vegetable name: ");
        String name = input.next();

        System.out.print("Enter amount of protein: ");
        double protein = input.nextDouble();

        System.out.print("Enter amount of fat: ");
        double fat = input.nextDouble();

        System.out.print("Enter amount of carbohydrates: ");
        double carbohydrates = input.nextDouble();

        Vegetable vegetableDetails = new Vegetable(name, protein, fat, carbohydrates);
        return vegetableDetails;
    }

    public static void printVegtables(Vegetable[] vegArr) {
        PrintWriter stdOut = null;
        try {
            stdOut = new PrintWriter(new FileOutputStream("VegCalories.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.print("Error! File VegCalories could not be created. Data not saved.");
        }

        stdOut.print("Vegetable" + "\t" + "\t" + "\t" + "\t");
        stdOut.println("Nutrition Info");
        stdOut.println("------------------------------------------------");

        for (int i = 0; i < vegArr.length; i ++) {
            stdOut.println(vegArr[i].veg_name);
            stdOut.print("% Protein: " + "\t" + "\t" + "\t" + "\t");
            stdOut.println(vegArr[i].getPercentProtein());
            stdOut.print("% Fat: " + "\t" + "\t" + "\t" + "\t" + "\t");
            stdOut.println(vegArr[i].getPercentFat());
            stdOut.print("% Carbohydrates: " + "\t" + "\t");
            stdOut.println(vegArr[i].getPercentCarbo());
            stdOut.print("Total Calories: " + "\t" + "\t");
            stdOut.println(vegArr[i].getCalories());
            stdOut.println("------------------------------------------------");
        }
        stdOut.close();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //System.out.print(inputVegetable().toSring());
        System.out.print("Enter number of vegetables: ");
        int num = input.nextInt();
        Vegetable[] vegArr = new Vegetable[num];
        for (int i = 0; i < num; i++) {
            vegArr[i] = inputVegetable();

        }
        printVegtables(vegArr);
    }
}
