package vegetables;

public class Vegetable {
    String veg_name;
    double veg_protein_cal, veg_fat_cal, veg_carbo_cal;

    public Vegetable() {
        this.veg_name = "";
        this.veg_protein_cal = 0.0;
        this.veg_fat_cal = 0.0;
        this.veg_carbo_cal = 0.0;
    }

    public Vegetable(String veg_name, double veg_protein_cal, double veg_fat_cal, double veg_carbo_cal) {
        this.veg_name = veg_name;
        this.veg_protein_cal = veg_protein_cal;
        this.veg_fat_cal = veg_fat_cal;
        this.veg_carbo_cal = veg_carbo_cal;
    }

    public double getCalories() {
        return this.veg_protein_cal + this.veg_fat_cal + this.veg_carbo_cal;
    }

    public double getPercentProtein() {
        return getCalories() / 100;
    }

    public double getPercentFat() {
        return (veg_fat_cal / 100) * getCalories();
    }

    public double getPercentCarbo() {
        return  ((veg_carbo_cal / 100) * getCalories() * 10000d) / 10000d;
    }

    public String toSring() {
        return (veg_name + ", " + getCalories() + "%, " + getPercentProtein() + "% " + getPercentFat() + "%, " + getPercentCarbo() + "%");
    }
}
